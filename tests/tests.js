/*var casper = require('casper').create({
    verbose: true
});*/

// Check if the login form exists.
casper.start('http://localhost:5000/', function() {
    if(this.exists('.login-form')) { this.echo('Found Login Form');}
});

// Fill in the login details
casper.then(function() {
    this.fill('form', {
        'email':    'utkarshsaxena93@gmail.com',
        'password':    'utk12345678'
    }, true);
});

// Verify that the user successfully logged in
casper.then(function() {
    this.test.assertEquals(this.getCurrentUrl(), 'http://localhost:5000/home');
});

// Fill in the login details
/*casper.then(function() {
    this.test.assertExists('.highcharts-container');
});*/

// Fill in the login details
casper.then(function() {
    this.fill('form', {
        'stockSymbol':    'AAL'
    }, true);
});

// Wait for chart to render
casper.waitForSelector('.highcharts-container');

// Verify that the chart exists
casper.then(function() {
     this.test.assertExists('.highcharts-container');
});

// Click on the manage alerts link
casper.then(function() {
    this.click('.alert-dashboard-btn');
});

// Verify that the user is on the dashboard page.
casper.then(function() {
    this.echo('Verify that the user is on the dashboard page.');
    this.test.assertEquals(this.getCurrentUrl(), 'http://localhost:5000/alertsystem');
});

// Click on "set-up trends" button
casper.then(function() {
    this.click('.btn.btn-danger');
});

// Wait for the form modal to render
casper.waitUntilVisible('#myModal');

// Select the first alert radio button
casper.then(function() {
    this.evaluate(function(){
        $('.alertone').click();      
    });
});

// Fill in the "set up trends form" details
casper.then(function() {
    this.fill('form', {
        'stock-symbol':    'AAL'
    }, false);
});

// Click on the submit button
casper.then(function() {
    this.click('.memory-alert-subscription');
});

casper.wait(2000);

// Verify the new trend is displayed
casper.then(function() {
    var trendAlert = this.evaluate(function(){
        return $('.trends-table tr td').text()      
    });
    this.test.assertEquals(trendAlert.indexOf('AAL'), 0);    
});

// Click on delete all trends btn
casper.then(function() {
    this.click('.delete-trends-btn');
});

 casper.setFilter('page.confirm', function(message) {
    self.received = message;
    this.echo("message to confirm : " + message);
    return true;
});

casper.wait(2000);

// Verify the trends have been deleted
casper.then(function() {
    var trendAlert = this.evaluate(function(){
        return $('.trends-table tr td').text()      
    });
    this.test.assertEquals(trendAlert.indexOf('AAL'), -1);    
});

// TO DO: Fill out the set up email trends and proceed to delete all trends.
// Click on "set-up trends" button
casper.then(function() {
    this.click('.btn.btn-success');
});

// Wait for the form modal to render
casper.waitUntilVisible('#myModalOne');

//Fill out the form
casper.then(function() {
    this.evaluate(function(){
        $('.moving-average-high').val(100);
        $('.moving-average-low').val(50);
        $('.notify-moving-average').click();
        $('.performance-relative-market').click();
        $('#marketCap').val(2000000);
        $('#stockPerformanceLow').val(0.9);
        $('#stockPerformanceHigh').val(1.1);      
    });
});

// Submit the form
casper.then(function() {
    this.click('.memory-notification-subscription');
});

casper.wait(2000);

// Verify the alerts are set up
casper.then(function() {
    var emailAlert = this.evaluate(function(){
        return $('.email-alerts tr td').text()     
    });
    this.test.assertNotEquals(emailAlert.indexOf('200000'), -1);    
});

// Click on delete all alerts btn
casper.then(function() {
    this.click('.delete-alerts-btn');
});

casper.wait(2000);

// Verify the alerts are set up
casper.then(function() {
    var emailAlert = this.evaluate(function(){
        return $('.email-alerts tr td').text()     
    });
    this.test.assertEquals(emailAlert.indexOf('200000'), -1);    
});

// Click on the logout button
casper.then(function() {
    this.click('.logout-btn');
});


// Verify that the user successfully logged out.
casper.then(function() {
    this.echo('Verify that the user successfully logged out.');
    this.test.assertEquals(this.getCurrentUrl(), 'http://localhost:5000/');
});

// Verify that the user successfully logged out.
casper.then(function() {
    this.test.done();
});

casper.run();
