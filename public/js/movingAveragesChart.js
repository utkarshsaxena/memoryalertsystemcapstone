// Code to display to moving averages whern requested by the user.

// Create a new plot for each stock symbol.
function createMovingAverages(stockSymbol, stockData){
    var i,
        l = stockSymbol.length,
        seriesObject = [],
        singleStockSymbol;
    for(i = 0; i<l; i++){
        singleStockSymbol = stockSymbol[i]
        seriesObject.push({
            name: singleStockSymbol,
            type: 'line',
            data : stockData[singleStockSymbol], // Separate data depending on the stock symbol
            tooltip: {
                valueDecimals: 2
            }            
        });        
    }
    return seriesObject;
};

// Determine which table to query
function getTableName(stockTableName) {
    if(stockTableName === "stockdata_1day") {
        return "movingaverage_1day";
    } else if (stockTableName === "stockdata_2weeks") {
        return "movingaverage_2weeks";        
    } else {
        return "movingaverage_1year"
    }
};

// Get data from DB
function getMovingAverages(symbols, sqlTable) { //change the ajax below and find a way to get stock symbols
    $.ajax({
        method: "GET",
        dataType: "json",
        url: "/api/getmovingaveragedata?stocksymbol=" + symbols + "&tablename=" + sqlTable,
        cache: false
    }).done(function( data ) {
        console.log(data);
        var stockSymbolArray = csvToArray(symbols);
        
        if (data && data[stockSymbolArray[0]][0]) {
            // if data exists
            $('.timer-loader-2').css('display','none');
            $('.show-moving-avg').css('display','inline-block');
        } else {
            $('.timer-loader-2').css('display','none');
            $('.stock-symbol-picker').addClass('has-error');
            $('.stock-symbol-picker input').after('<label class="control-label" for="inputError1">No data found for moving averages!</label>');
            return false;            
        }
        // create the chart
        $('#moving-avg-chart').highcharts('StockChart', {
            title: {
                text: 'Moving Averages'
            },
            chart: {
                zoomType : 'x',
                panKey: 'shift'               
            },
            rangeSelector : {
                selected : 3, // Select All data by default
                inputEnabled : false // do not show the date range
            },
            series: createMovingAverages(stockSymbolArray, data) // return seriesObject
        });
    });
};

// Handle click event on "Show Moving Average" button

$('.show-moving-avg').on('click', function(event){
	
	event.preventDefault();
	
	// Disable button till data is returned
	$('.show-moving-avg').attr('disabled','disabled');
	
	// Init variables
	var tableName = getTableName(window.tableName),
		symbols = window.stockSymbol;
	
	// Fetch Data
	getMovingAverages(symbols, tableName);	
});