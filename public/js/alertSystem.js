/* global $ */
// Alery system specific JS


// Initialize jquery-tagit
$(document).ready(function() {
    // Get trends auto complete
    $.ajax({
        method: "GET",
        url: "/api/getstocksymbols",
        cache: true
    }).done(function( data ) {
        $("#stock-tags").tagit({
            tagLimit: 1,
            placeholderText: "Enter a stock symbol",
            removeConfirmation: true,
            beforeTagAdded: function(event, ui) {
                // Only add tag if it exists in the array
                if($.inArray(ui.tagLabel, data)==-1) return false;        
            },
            autocomplete: {
                delay: 0,
                minLength: 1,
                source: data,
                appendTo: "#autocomplete-container"
            }
        });
    });   
    
    $.ajax({
        method: "GET",
        url: "/api/allalerts?userid=" + $('.memory-alert-subscription').attr('data-userid')
    }).done(function( data ) {
        if (data[0]) {            
            // Iterate though all indices and max ajax call to display data.
            for(var i = 0, l =  data.length; i < l; i++) {
                if (data[i].alertOne === 'true') {
                    $.ajax({
                        method: "GET",
                        url: "/api/alertsData?stocksymbol=" + data[i].stockSymbols + "&alertone=" + data[i].alertOne + "&alerttwo=" + data[i].alertTwo + "&alertthree=" + data[i].alertThree
                        }).done(function( result ) {
                            if (result[0]) {
                                var htmlContent = [
                                    '<tr>',
                                    '<td>' + result[0].StockName + ': ' +  result[0].MarketValue.toFixed(4)  + '</td>',
                                    '<td>',
                                    'Market cap is larger than 500 M',
                                    '</td>',
                                    '</tr>'
                                    ];
                                $('.trends-table').append(htmlContent.join(''));                        
                            }
                        });                    
                } else if (data[i].alertTwo === 'true') {
                     $.ajax({
                        method: "GET",
                        url: "/api/alertsData?stocksymbol=" + data[i].stockSymbols + "&alertone=" + data[i].alertOne + "&alerttwo=" + data[i].alertTwo + "&alertthree=" + data[i].alertThree + "&alertfour=" + data[i].alertFour + "&alertfive=" + data[i].alertFive
                        }).done(function( result ) {
                            if (result[0]) {
                                var htmlContent = [
                                    '<tr>',
                                    '<td>' + result[0].StockName + ': ' +  result[0].MarketValue.toFixed(4)  + '</td>',
                                    '<td>',
                                    'Market cap is larger than 200 M',
                                    '</td>',
                                    '</tr>'
                                    ];
                                $('.trends-table').append(htmlContent.join(''));                        
                            }
                        });
                    }  else if (data[i].alertThree === 'true') {
                     $.ajax({
                        method: "GET",
                        url: "/api/alertsData?stocksymbol=" + data[i].stockSymbols + "&alertone=" + data[i].alertOne + "&alerttwo=" + data[i].alertTwo + "&alertthree=" + data[i].alertThree + "&alertfour=" + data[i].alertFour + "&alertfive=" + data[i].alertFive
                        }).done(function( result ) {
                            if (result[0]) {
                                var htmlContent = [
                                    '<tr>',
                                    '<td>' + result[0].StockName + '<br>' +  'Stock Performance: ' + result[0].StockPerformance.toFixed(4)  + '<br>' + 'Performance Relative to Market: ' + result[0].StockPerformanceRelativeToMarket.toFixed(4) + '</td>',
                                    '<td>',
                                    'One Day Performance of the stock',
                                    '</td>',
                                    '</tr>'
                                    ];
                                $('.trends-table').append(htmlContent.join(''));                        
                            }
                        });
                    } else if (data[i].alertFour === 'true') {
                        $.ajax({
                            method: "GET",
                            url: "/api/alertsData?stocksymbol=" + data[i].stockSymbols + "&alertone=" + data[i].alertOne + "&alerttwo=" + data[i].alertTwo + "&alertthree=" + data[i].alertThree + "&alertfour=" + data[i].alertFour + "&alertfive=" + data[i].alertFive
                            }).done(function( result ) {
                                if (result[0]) {
                                    var htmlContent = [
                                        '<tr>',
                                        '<td>' + result[0].StockName + '<br>' +  'Stock Performance: ' + result[0].StockPerformance.toFixed(4)  + '<br>' + 'Performance Relative to Market: ' + result[0].StockPerformanceRelativeToMarket.toFixed(4) + '</td>',
                                        '<td>',
                                        'Three Days Performance of the stock',
                                        '</td>',
                                        '</tr>'
                                        ];
                                    $('.trends-table').append(htmlContent.join(''));                        
                                }
                            });
                    } else if (data[i].alertFive === 'true') {
                        $.ajax({
                            method: "GET",
                            url: "/api/alertsData?stocksymbol=" + data[i].stockSymbols + "&alertone=" + data[i].alertOne + "&alerttwo=" + data[i].alertTwo + "&alertthree=" + data[i].alertThree + "&alertfour=" + data[i].alertFour + "&alertfive=" + data[i].alertFive
                            }).done(function( result ) {
                                if (result[0]) {
                                    var htmlContent = [
                                        '<tr>',
                                        '<td>' + result[0].StockSymbol + '<br>' +  '5 days: ' + result[0]['5day'].toFixed(4)  + '<br>' + '10 days: ' + result[0]['10day'].toFixed(4)  + '<br>' +  '20 days: ' + result[0]['20day'].toFixed(4)  + '<br>' + '50 days: ' + result[0]['50day'].toFixed(4) + '<br>' + '100 days: ' + result[0]['100day'].toFixed(4)  +  '<br>' + '200 days: ' + result[0]['200day'].toFixed(4)  + '</td>',
                                        '<td>',
                                        'Three Days Performance of the stock',
                                        '</td>',
                                        '</tr>'
                                        ];
                                    $('.trends-table').append(htmlContent.join(''));                        
                                }
                            });
                    }
                }
            }
    });   
});


// Get all email alerts to display to the user
var userEmail = $('.email-alerts').attr('data-useremail');

$.ajax({
    method: "GET",
    url: "/api/emailalertsdata?email=" + userEmail
    }).done(function( result ) {
        if (result[0]) {
            for (var i = 0, l = result.length; i< l; i++) {
                var htmlContent = [
                    '<tr>',
                    '<td>',
                    'Market Cap : ' + result[i].MarketCap + '<br>' + 'Market Cap higher: ' + result[i].MarketCapHigher + '<br>' + 'Notify Market Cap: ' + result[i].NotifyMarketCap + '<br>' +  'Notify Moving Average: ' + result[i].NotifyMovingAverage + '<br>',
                    'Notify Stock Performance: ' + result[i].NotifyStockPerformance + '<br>' + 'Stock Performance High Range: ' + result[i].StockPerformanceHighRange + '<br>' + 'Stock Performance Low Range: ' + result[i].StockPerformanceLowRange + '<br>',
                    'Stock Performance Relative To Market: ' + result[i].StockPerformanceRelativeToMarket,
                    '</td>',
                    '</tr>'
                ];
                
                $('.email-alerts').append(htmlContent.join(''));
            }
        }
    });

// Handle updating alert subscriptions
$('.memory-alert-subscription').on('click', function(event){
    event.preventDefault();
    var formData = {
        stockSymbols: $('.stock-symbol-picker input').val(),
        alertOne: $('.alertone')[0].checked,
        alertTwo: $('.alerttwo')[0].checked,
        alertThree: $('.alertthree')[0].checked,
        alertFour: $('.alertfour')[0].checked,
        alertFive: $('.alertfive')[0].checked,
        userid: $(event.target).attr('data-userid')
    };
    
    if ((formData.stockSymbols && formData.alertOne) || (formData.stockSymbols && formData.alertTwo) || (formData.stockSymbols && formData.alertThree) || (formData.stockSymbols && formData.alertFour) || (formData.stockSymbols && formData.alertFive)) {
        $.ajax({
            method: "POST",
            dataType: "json",
            url: "/api/setupalerts",
            data: formData
        }).done(function( data ) {
            if(data.success) {
                location.reload();
            }
        });       
    } else {
        $('.text-danger').text('Invalid Form. Please fill the required field.');
    }
    
      
});

// Handle updating notifications subscriptions
$('.memory-notification-subscription').on('click', function(event){
    event.preventDefault();
    var formData = {
        movingAverageHigh: $('.moving-average-high').val(),
        movingAverageLow: $('.moving-average-low').val(),
        notifyMovingAverage: $('.notify-moving-average')[0].checked || false,
        notifyStockPerformance: $('.notify-stock-performance')[0].checked || false,
        performanceRelativeMarket: $('.performance-relative-market')[0].checked || false,
        marketCapHigh: $('.market-cap-high')[0].checked || false,
        marketCapVal: $('#marketCap').val() || null,
        stockPerformanceLowVal: $('#stockPerformanceLow').val() || null,
        stockPerformanceHighVal: $('#stockPerformanceHigh').val() || null,        
        userEmail: $(event.target).attr('data-email')
    };
    
     if(formData.marketCapVal) {
        formData.notifyMarketCap = true;
    } else {
        formData.notifyMarketCap = false;
    }
    
    $.ajax({
        method: "POST",
        dataType: "json",
        url: "/api/setupemailalerts",
        data: formData
    }).done(function( data ) {
        location.reload();
    });                  
   
});

// Handle delete trends btn
$('.delete-trends-btn').on('click', function(event){
    event.preventDefault();
    var userid = $('.trends-table').attr('data-userid');
    var confirmMsg = confirm('Are you sure you want to delete all trends?');
    
    if (confirmMsg) {
         $.ajax({
            method: "get",
            url: "/api/deletealltrends?userid=" + userid
        }).done(function( data ) {
            location.reload();
        });                  
    } 
});

// Handle delete email alerts btn

$('.delete-alerts-btn').on('click', function(event){
    event.preventDefault();
    
    var confirmMsg = confirm('Are you sure you want to delete all email alerts?');
    
    if (confirmMsg) {
         $.ajax({
            method: "get",
            url: "/api/deleteallalerts?email=" + userEmail
        }).done(function( data ) {
            location.reload();
        });                  
    } 
});