/* global $ */
// Display random data from highcharts.js'

var d = new Date(),
        todaysDate = d.getTime();
        
// Helper functions
function dateToMilliSeconds(date) {
    var myDate = new Date(date);
    return myDate.getTime();	
};

// Create a new plot for each stock symbol.
function createPlotForSymbols(stockSymbol, stockData){
    var i,
        l = stockSymbol.length,
        seriesObject = [],
        singleStockSymbol;
    for(i = 0; i<l; i++){
        singleStockSymbol = stockSymbol[i]
        seriesObject.push({
            name: singleStockSymbol,
            type: 'candlestick',
            data : stockData[singleStockSymbol], // Separate data depending on the stock symbol
            tooltip: {
                valueDecimals: 2
            }            
        });        
    }
    return seriesObject;
};

// Create Table name string

function getStockTableName(stockSymbol) {
    var stockSymbolArr = stockSymbol.split(',');
    var stockInfo = window.stockInfo;
    var stockTableArr = [];
    
    for (var j=0, k=stockSymbolArr.length; j <k; j++) {
        for(var i=0, l=stockInfo.length; i<l; i++) {
            if(stockInfo[i].Symbol == stockSymbolArr[j]) {
                stockTableArr.push(stockInfo[i].Sector + '_' + stockInfo[i].Industry);         
            }
        }       
    }
    return stockTableArr;    
}
// CSV to array
function csvToArray(csv) {
    return csv.split(',').sort();
};

// Plot Stock Data
function plotStockData(symbols, sqlTable, startDate, endDate) {
    
    $.ajax({
        method: "GET",
        dataType: "json",
        url: "/api/getstocksymboldata?stocksymbol=" + symbols + "&tablename=" + sqlTable,
        cache: false
    }).done(function( data ) {
        var stockSymbolArray = csvToArray(symbols);
        if (data && data[stockSymbolArray[0]][0]) {
            // if data exists
            $('.timer-loader').css('display','none');
            $('.show-moving-avg').css('display','inline-block');
        } else {
            $('.timer-loader').css('display','none');
            $('.stock-symbol-picker').addClass('has-error');
            $('.stock-symbol-picker input').after('<label class="control-label" for="inputError1">No data found. Please enter the correct industry and sector for the stock.</label>');
            return false;            
        }
        // create the chart
        $('#stock-chart').highcharts('StockChart', {
            title: {
                text: 'Memory Alert System'
            },    
            rangeSelector : {
                buttons : [{
                    type : 'hour',
                    count : 1,
                    text : '1h'
                }, {
                    type : 'day',
                    count : 1,
                    text : '1D'
                }, {
                    type : 'all',
                    count : 1,
                    text : 'All'
                }],
                selected : 3, // Select All data by default
                inputEnabled : false // do not show the date range
            },
            chart: {
                zoomType : 'x',
                panKey: 'shift'               
            },
            yAxis: {
                endOnTick: false,
                startOnTick: false
            },
            series: createPlotForSymbols(stockSymbolArray, data) // return seriesObject
        });
        
        if (startDate && endDate){
            var chartz = $('#stock-chart').highcharts();
            chartz.xAxis[0].setExtremes(
                dateToMilliSeconds(startDate),
                dateToMilliSeconds(endDate)
            );            
        }
        
        // Hide link to highcharts
        $('text').last().hide();
    });
};

// Validate form data
function validate(stockSymbol, startDate, endDate){
    if(stockSymbol.length < 1) {
        $('.stock-symbol-picker').addClass('has-error');
        $('.stock-symbol-picker').after('<label class="control-label inputError" for="inputError1">Please enter a symbol.</label>');
        return false;
    }
    if(!startDate || !endDate) {
        $('#datetimepicker6').addClass('has-warning');
        $('#datetimepicker7').addClass('has-warning');
        $('#datetimepicker6').after('<label class="inputWarning">No date range specified.</label>');
        $('#datetimepicker7').after('<label class="inputWarning">Fetching all data</label>');                    
    } else {        
         if(dateToMilliSeconds(startDate) > dateToMilliSeconds(endDate)) {
            $('#datetimepicker6').addClass('has-error');
            $('#datetimepicker7').addClass('has-error');
            $('#datetimepicker6').after('<label class="inputError">Invalid Date Range</label>');
            $('#datetimepicker7').after('<label class="inputError">Start date should be before end date.</label>');
            return false;
        }        
        if(dateToMilliSeconds(startDate) > todaysDate) {
            $('#datetimepicker6').addClass('has-error');
            $('#datetimepicker7').addClass('has-error');
            $('#datetimepicker6').after('<label class="inputError">Invalid Date Range</label>');
            $('#datetimepicker7').after('<label class="inputError">Start date should be before today\'s date.</label>');
            return false;
        }
    }
    return true;
};

// Clean all previous errors
function sanatizeForm(){
     $('.stock-symbol-picker').removeClass('has-error');
     $('.date').removeClass('has-warning');
     $('.date').removeClass('has-error');
     $('.inputWarning').remove();
     $('.control-label').remove();
     $('.inputError').remove();   
};

// Handle user input and render a chart
$('.user-input').submit(function(event){
    
    // Remove all previous errors/warning
    sanatizeForm();
    
    event.preventDefault();
    
    // Get form data
    var startDate = $('#datetimepicker6 input').val(),
        endDate = $('#datetimepicker7 input').val(),
        isFormValid;
        
    // Set form data as global variables. To be used in movingAveragesChart.js
    window.stockSymbol = $('.stock-symbol-picker input').val(); // Returns CSV
    window.tableName =   getStockTableName(window.stockSymbol);
    // Validate form data 
    isFormValid = validate(window.stockSymbol, startDate, endDate);
     
    if(isFormValid) {
        $('.timer-loader').css('display','inline-block');
        plotStockData(window.stockSymbol, window.tableName.join(), startDate, endDate);
    }        
});