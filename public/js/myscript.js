/* global $ */
// Main js file.

// Initialize jquery-tagit
$(document).ready(function() {
    $.ajax({
        method: "GET",
        url: "/api/getstocksymbols",
        cache: true
    }).done(function( data ) {
        $("#stock-tags").tagit({
            tagLimit: 5,
            placeholderText: "Enter a stock symbol",
            removeConfirmation: true,
            beforeTagAdded: function(event, ui) {
                // Only add tag if it exists in the array
                if($.inArray(ui.tagLabel, data)==-1) return false;        
            },
            autocomplete: {
                delay: 0,
                minLength: 1,
                source: data,
                appendTo: "#autocomplete-container"
            }
        });
    });
});

// Initialize jquery-tagit
$(document).ready(function() {
    $.ajax({
        method: "GET",
        url: "/api/getstockindustrysector",
        cache: true
    }).done(function( data ) {
        window.stockInfo = data;
    });
});

// Initialize the date time picker for the homepage
$(function () {
    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:SS'
        });
            $('#datetimepicker7').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:SS'
            });
    });
});