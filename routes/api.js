// This file contains the primary routes for the platform
module.exports = function(express, app, passport, con, cache, Users) {
	var router = express.Router();
	
	// Route middlewares to check if a user is logged in
	var isLoggedIn = require('./middlewares.js').isLoggedIn;
	
	// Helper functions
	function dateToMilliSeconds(date) {
		var myDate = new Date(date);
		return myDate.getTime();	
	};
	
	// CSV to array
	function csvToArray(csv) {
		return csv.split(',').sort();
	};
    
    function csvToUnsortArray(csv) {
		return csv.split(',');
	};
	
	// Query Builder
	function buildQuery(stockSymbol, tableName){		
		// Initialize variables
		var query = [],
            index = 0;
		
		// Enter each part of the string into the array
        for (var i=0, l=stockSymbol.length; i<l; i++) {
            query[index++] = "SELECT * FROM";
            query[index++] = ' ' + tableName[i];
            query[index++] = ' WHERE StockSymbol = "' + stockSymbol[i] + '"';
            query[index++] = ' GROUP BY TradingDate'
            if (i === l-1) {
                //query[index++] = ' ORDER BY stocksymbol,TradingDate ASC';               
            } else {
                 query[index++] = ' UNION ';
            }                                       
        }
		// Join the array to form the query string. Return the string
        return query.join('');	
	};
	
	// =====================================
    // GET ALL STOCK SYMBOLS
    // =====================================
	router.get('/getstocksymbols', isLoggedIn, function(req, res){
        cache.fetch('stocksymbols').otherwise(function(deferred, cacheKey) {			
            con.query("SELECT Symbol FROM stockcategory GROUP BY Symbol", function(err, rows){
                // Handle error
                if(err) {
                    deferred.reject(err);
                }
                var i,
                    l = rows.length,
                    stockSymbol = [];
                for(i = 0; i < l; i++) {
                    stockSymbol.push(rows[i].Symbol);
                }
                deferred.resolve(stockSymbol);
		      });			
		}).then(function(data) {
			res.send(data);
			
		}).fail(function(err) {			
			// Invoked when you reject the promise above. 
			console.log(err);		
		});	
	});
    
    // =====================================
    // GET ALL STOCK SYMBOLS
    // =====================================
	router.get('/getstockindustrysector', isLoggedIn, function(req, res){
        cache.fetch('stockindustrysector').otherwise(function(deferred, cacheKey) {			
            con.query("SELECT * FROM stockcategory", function(err, rows){
                // Handle error
                if(err) {
                    deferred.reject(err);
                }
                deferred.resolve(rows);
		      });			
		}).then(function(data) {
			res.send(data);
			
		}).fail(function(err) {			
			// Invoked when you reject the promise above. 
			console.log(err);		
		});	
	});
	
	// =====================================
    // HANDLE STOCK SYMBOL DATA
    // =====================================
	router.get('/getstocksymboldata', function(req, res){
		var stockSymbol = csvToUnsortArray(req.query.stocksymbol),
            stockTables = csvToUnsortArray(req.query.tablename),
			sqlQuery = buildQuery(stockSymbol, stockTables);
			
		cache.fetch(stockSymbol.toString() + req.query.tablename).otherwise(function(deferred, cacheKey) {
			
			// Go to SQL and get the required data
			con.query(sqlQuery.toLowerCase(), function(err, rows){
					// Handle error
					if(err) {
						deferred.reject(err);
					}
					// Convert data into plottable data
					var i,
						l = stockSymbol.length,
						j = 0,
						k = rows.length,
						allData = {},
						singleStockData;
							
					for ( i = 0 ; i < l ; i++) {
						allData[stockSymbol[i]] = [];
						for( j; j < k; j++) {
							if(rows[j].StockSymbol == stockSymbol[i]) {
								singleStockData = [];
								singleStockData.push(dateToMilliSeconds(rows[j].TradingDate));
								//singleStockData.push(rows[j].TradingVolume); //This data point needs to be plotted differently
								singleStockData.push(rows[j].TradingOpen);
								singleStockData.push(rows[j].TradingHigh);
								singleStockData.push(rows[j].TradingLow);
								singleStockData.push(rows[j].TradingClose);
								singleStockData.push(rows[j].StockSymbol);
								allData[stockSymbol[i]].push(singleStockData);				
							} else {
								break;
							}
						}
					}
					deferred.resolve(allData);
			});
		}).then(function(data) {
			res.send(data);
			
		}).fail(function(err) {			
			// Invoked when you reject the promise above. 
			console.log(err);		
		});
	});
	
	// =====================================
    // HANDLE MOVING AVERAGES DATA
    // =====================================
	router.get('/getmovingaveragedata', function(req, res){
		var stockSymbol = csvToArray(req.query.stocksymbol),
			sqlQuery = buildQuery(stockSymbol, req.query.tablename);
			
		cache.fetch(stockSymbol.toString() + req.query.tablename).otherwise(function(deferred, cacheKey) {
			
			// Go to SQL and get the required data
			con.query(sqlQuery, function(err, rows){
					// Handle error
					if(err) {
						deferred.reject(err);
					}
					// Convert data into plottable data
					var i,
						l = stockSymbol.length,
						j = 0,
						k = rows.length,
						allData = {},
						singleStockData;
							
					for ( i = 0 ; i < l ; i++) {
						allData[stockSymbol[i]] = [];
						for( j; j < k; j++) {
							if(rows[j].StockSymbol == stockSymbol[i]) {
								singleStockData = [];
								singleStockData.push(dateToMilliSeconds(rows[j].TradingDate));
								singleStockData.push(rows[j]['5day']);
								/*singleStockData.push(rows[j]['10day']);
								singleStockData.push(rows[j]['20day']);
								singleStockData.push(rows[j]['50day']);
								singleStockData.push(rows[j]['100day']);
								singleStockData.push(rows[j]['200day']);*/
								allData[stockSymbol[i]].push(singleStockData);				
							} else {
								break;
							}
						}
					}
					deferred.resolve(allData);
			});
		}).then(function(data) {
			res.send(data);
			
		}).fail(function(err) {			
			// Invoked when you reject the promise above. 
			console.log(err);		
		});
	});
    
    // =====================================
    // SET UP ALERTS
    // =====================================
    router.post('/setupalerts', isLoggedIn, function(req, res){
        Users.update(
                        {
                            _id : req.body.userid
                        },
                        { 
                            $push:{
                                 alerts : req.body
                            }
                        },
                        {
                            new : true
                        },
                        function(err, data) {
                            if (err) {
                                res.send({success: false, msg: err});
                            }
                            
                            if(data) {
                                res.send({success: true, data: data});
                            }
                        });
	});
    
    
    router.post('/setupemailalerts', function(req, res){
        var query = [];
        query[0] = "INSERT INTO notification ( Email, MarketCap, MarketCapHigher, StockPerformanceLowRange, StockPerformanceHighRange,";
        query[1] = " StockPerformanceRelativeToMarket, NotifyStockPerformance, NotifyMovingAverage, NotifyMarketCap, XDayMovingAverageHigher, XDayMovingAverageLower ) VALUES (";
        query[2] = "'" + req.body.userEmail + "', " + req.body.marketCapVal + ', ' + req.body.marketCapHigh + ', ' + req.body.stockPerformanceLowVal + ', ';
        query[3] = req.body.stockPerformanceHighVal + ', ' + req.body.performanceRelativeMarket + ', ' + req.body.notifyStockPerformance + ', ' + req.body.notifyMovingAverage + ', ';
        query[4] = req.body.notifyMarketCap + ', ' + req.body.movingAverageHigh + ', ' + req.body.movingAverageLow + ')';
        
        con.query(query.join('') , function(err, result){
					// Handle error
					if(err) {
						console.log(err);
					}					
					res.send(result);
			}); 
	});
        
    router.get('/allalerts', isLoggedIn, function(req, res){
       Users.findById(req.query.userid).exec(function(err, user){
            if(err) {
				console.log("An error occurred while deleting this project: " + err.message);
			}
            res.send(user.alerts);
        });
	});
    
    router.get('/emailalertsdata', isLoggedIn, function(req, res){
        var email = req.query.email;
        con.query("SELECT * FROM notification WHERE email ='" + email + "'", function(err, rows){
					// Handle error
					if(err) {
						console.log(err);
					}					
					res.send(rows);
			});       
	});
    
    router.get('/deleteallalerts', isLoggedIn, function(req, res){
        var email = req.query.email;
        con.query("DELETE FROM notification WHERE email ='" + email + "'", function(err, result){
					// Handle error
					if(err) {
						console.log(err);
					}					
					res.send(result);
			});       
	});
    
    router.get('/deletealltrends', isLoggedIn, function(req, res){
         Users.update(
                        {
                            _id : req.query.userid
                        },
                        { 
                            $set:{
                                 alerts : []
                            }
                        },
                        {
                            new : true
                        },
                        function(err, data) {
                            if (err) {
                                res.send({success: false, msg: err});
                            }
                            
                            if(data) {
                                res.send({success: true, data: data});
                            }
                        });      
	});
    
    router.get('/alertsData', isLoggedIn, function(req, res){
       // Go to SQL and get the required data
       if(req.query.alertone === "true") {
           con.query("SELECT * FROM marketcap WHERE (StockName ='" + req.query.stocksymbol + "') AND (MarketValue > 500)", function(err, rows){
					// Handle error
					if(err) {
						console.log(err);
					}					
					res.send(rows);
			});           
       } else if (req.query.alerttwo === "true") {
           con.query("SELECT * FROM marketcap WHERE (StockName ='" + req.query.stocksymbol + "') AND (200 < MarketValue < 500)", function(err, rows){
					// Handle error
					if(err) {
						console.log(err);
					}					
					res.send(rows);
			});           
       } else if (req.query.alertthree === "true") {
           con.query("SELECT * FROM stockperformance1day WHERE (StockName ='" + req.query.stocksymbol + "') GROUP BY " +  "'" + req.query.stocksymbol + "'", function(err, rows){
					// Handle error
					if(err) {
						console.log(err);
					}					
					res.send(rows);
			});           
       } else if (req.query.alertfour === "true") {
           con.query("SELECT * FROM stockperformance3days WHERE (StockName ='" + req.query.stocksymbol + "') GROUP BY " +  "'" + req.query.stocksymbol + "'", function(err, rows){
					// Handle error
					if(err) {
						console.log(err);
					}					
					res.send(rows);
			});           
       } else if (req.query.alertfive === "true") {
           con.query("SELECT * FROM movingaverage WHERE (StockSymbol ='" + req.query.stocksymbol + "') GROUP BY StockSymbol", function(err, rows){
					// Handle error
					if(err) {
						console.log(err);
					}					
					res.send(rows);
			});           
       }		
	});
	
	// mount the router on the app
	// All routes relative to '/'
	app.use('/api', router);
}