// This file contains all the middlewares for the web application
var middlewares = {};

middlewares.isLoggedIn = function isLoggedIn(req, res, next) {
							if(req.isAuthenticated()) {
								req.session.userLoggedIn = true;
								return next();
							} else {
								req.session.userLoggedIn = false;
							}
							res.redirect('/');
						}


module.exports = middlewares;