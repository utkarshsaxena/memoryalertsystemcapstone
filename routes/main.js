// This file contains the primary routes for the platform
module.exports = function(express, app, passport, con, cache, Users) {
	var router = express.Router();
	
	// Route middlewares to check if a user is logged in
	var isLoggedIn = require('./middlewares.js').isLoggedIn;
	
	// =====================================
    // LOG-IN PAGE
    // =====================================
	router.get('/', function(req, res){
		res.render('index', {
			isUserLoggedIn: req.session.userLoggedIn || false,
			errorMessage: req.flash('loginMessage')
		});
	});
	
	router.post('/', passport.authenticate('local-login', {
		successRedirect: '/home',
		failureRedirect: '/',
		failureFlash: true // Show flash messages
	}));
	
	// =====================================
    // SIGN-UP PAGE
    // =====================================
	router.get('/signup', function(req, res){
		res.render('signup', {
			isUserLoggedIn: req.session.userLoggedIn || false,
			errorMessage: req.flash('signupMessage')
		});
	});
	
	router.post('/signup', passport.authenticate('local-signup', {
		successRedirect: '/home',
		failureRedirect: '/signup',
		failureFlash: true // Show flash messages
	}));
	
	// =====================================
    // HOME PAGE
    // =====================================	
	router.get('/home', isLoggedIn, function(req, res){
		res.render('home', {
			isUserLoggedIn: req.session.userLoggedIn,
			user : req.user
		});
	});
    
    // =====================================
    // ALERT SYSTEM PAGE
    // =====================================	
	router.get('/alertsystem', isLoggedIn, function(req, res){
		res.render('alertsystem', {
			isUserLoggedIn: req.session.userLoggedIn,
			user : req.user
		});
	});
	
	// =====================================
    // LOGOUT
    // =====================================	
	router.get('/logout', function(req, res){
		req.session.userLoggedIn = false;
		req.logout();
		res.redirect('/');
	});
	
	// Require the routes for the API
	require('./api')(express, app, passport, con, cache, Users);
	
	// mount the router on the app
	// All routes relative to '/'
	app.use('/', router);
}