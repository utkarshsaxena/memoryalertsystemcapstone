# README #

## Introduction:  

This is a web application created by group #99 as a part of our final year capstone project for our client - Patternly Analytics based in Richmond, BC, Canada

UBC Capstone - Web Application - Memory System Analysis


## Technologies: ##

* HTML
* CSS/SASS
* Javascript
* NodeJS
* MongoDB
* SQL
* Redis
* Amazon Web Services

## Purpose: ##

* This is an exercise in creating a project based on client requirements and to gain experience in professionalism and working with clients.

To run the website locally:

1. Download all the files locally.

2. Install NodeJS (https://nodejs.org/en), Redis (http://redis.io/) on your machine.

3. Open command prompt from the root folder where you copied the files.

4. Run the following commands in order:

npm install
redis-server --maxheap 2gb
node app.js

5. Browse to localhost:5000 on your machine to view the application.

## Timestamp: ##

**September, 2015 – April, 2016