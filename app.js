/* global __dirname */
/* global process */
// Get all modules
var express = require('express'),
	app = express(),
	cons = require('consolidate'), // Templating library adapter for Express
	path =  require('path'),
	mongoose = require('mongoose'),
	swig = require('swig'),
	passport = require('passport'),
	flash = require('connect-flash'),
	morgan = require('morgan'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	session = require('express-session'),
	ConnectMongo = require('connect-mongo')(session),
	mysql = require('mysql'),
	redis = require('redis'),
	cache = require('rediscache'),
	port = process.env.PORT || 5000,
	config = require('./config/config'),
    Users = require('./models/user');


// Get environment variable
var env = process.env.NODE_ENV || 'development';

if(env === 'development') {
	console.log(config.message);
	// Express middleware to populate 'req.cookies' so we can access cookies
    // Set up our application
	app.use(morgan('dev')); // Log requests to console
	app.use(cookieParser()); // Read cookies for authentication
    app.use(session({
        secret: config.sessionSecret,
        saveUninitialized: true,
        resave: true,
        cookie: {}
    })); // saving cookies to session locally
	
	// Connection to mySQL database
	var con = mysql.createConnection({
		host     : config.mysql.host,
		user     : config.mysql.user,
		password : config.mysql.password,
		database : config.mysql.database
	});
	
	// Connect to Redis database
	var client = redis.createClient();
	client.on('connect', function() {
		console.log('Connected to Redis');
		// Connect Redis cache
		cache.connect().configure({
			expiry: 21600
		});
	});
	
	
	
} else {
	// Set up our application
	app.use(cookieParser()); // Read cookies for authentication
    app.use(session({
        secret: config.sessionSecret,
        saveUninitialized: true,
        resave: true,
        store: new ConnectMongo({
            url: config.dbURL,
            stringify: true        
        })    
    })); // Saving cookies to mongodb when in production
	
	// Connection to mySQL database
	var con = mysql.createConnection({
		host     : config.mysql.host,
		user     : config.mysql.user,
		password : config.mysql.password,
		database : config.mysql.database
	});
	
	
	// Connect to Redis database
	var client = redis.createClient(config.redis.URL);
	client.on('connect', function() {
		console.log('Connected to Redis');
		// Connect Redis cache
		cache.connect(config.redis.port, config.redis.host, config.redis.pass).configure({
			expiry: 21600
		});
	});
	
	console.log(config.message);
}

// Resolve paths to client files.
var client_files = path.resolve(__dirname, './client/');

// Connect to MongoDB
mongoose.connect(config.dbURL, function(err, db) {
	if(err) {
        console.log('Error at mongoose.connect');
        console.error(err);
    };
	
	// Connect to mySQL database
	con.connect(function(err){
		if (err) {
			console.log('error connecting: ' + err.stack);
			return;
		}
		console.log('connected to mySQL as id '+ con.threadId);		
	});
	
	// Pass in passport configuration file
	// Needs to be configured before sending to routes
	require('./config/passport')(passport);
	
	// Register our templating engine
	app.engine('html', cons.swig);
	app.set('view engine', 'html');
	app.set('views', __dirname + '/views');
	
	// Express middleware to serve static files
	app.use(express.static('public'));
	
	// Express middleware to populate 'req.body' so we can access POST variables
	app.use(bodyParser.json()); // Parsing html forms
	app.use(bodyParser.urlencoded({ extended: true }));
	
	// Set up passport
	app.use(passport.initialize());
	app.use(passport.session()); // Persisting log-in sessions
	app.use(flash()); // For flash messages stored in session

	// Get routes from routes/main.js
	require('./routes/main.js')(express,app, passport, con, cache, Users);
	
	app.listen(port, function() {
		console.log('Node app is running on port',
		port);
	});	
});